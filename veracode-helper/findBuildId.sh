#!/bin/bash

# enter the file name you want to search
file="../policy_scan_output.txt"

# enter the pattern you want to search for
pattern="The analysis id of the new analysis is"

# search for the pattern in the file and extract the matching line
line=$(grep "${pattern}" "${file}" | head -n 1)

# split the line into an array using the delimiter
IFS="\""

set $line
buildId=$2
echo $2