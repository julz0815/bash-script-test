/*
command args
1 - Gitlab Private token
2 - Issue generation true|false
3 - Gitlab project ID - integer
4 - create a new issue if the previous is in stat closed, but the issue still shows up from the scan true|false
*/

const fs = require('fs');
const Axios = require('axios');
const Math = require('mathjs');
const { rangeTransformDependencies } = require('mathjs');

const scaInputFileName = 'scaResults.json'; // 'results.json'
const GitlabOutputFileName = 'output-sca-vulnerabilites.json'; // 'veracode-results.json'
var vulns=[];
var vulnerabilities=[];
var remeds=[];
var remediations=[];
var mapSeverity = "";

var myArgs = process.argv.slice(2);



const convertSCAResultFileToJSONReport = async (inputFileName,outputFileName) => {
    var results = {};
    var vulnResults={};
    var listIssueResponse={};

    var rawdata = fs.readFileSync(inputFileName);
    results = JSON.parse(rawdata);
    console.log('SCA Scan results file found and parsed - validated JSON file');

    var issues = results.records[0].vulnerabilities;
    numberOfVulns = issues.length
    console.log('Vulnerabilities count: '+issues.length);


    // check for existing issue and store results in listIssueResponse
    var listIssueResponse = [];
    if (myArgs[1] == 'true'){
      const getOldIssues = async (page) => {
        try {
          var issueResponse = await Axios.request({
            method: 'GET',
            headers:{
                'PRIVATE-TOKEN': myArgs[0]
            },
            url: 'https://gitlab.com/api/v4/projects/'+myArgs[2]+'/issues?page='+page+'&per_page=100&labels=Dependency%20Scanning'
          });
          console.log('Exisiting issues found - page '+page)
          return issueResponse.data
        }
        catch (e) {
          console.log(e)
        }
      }

      var page = 1
      var m = 0
      while (m < page) {
        var newResults = await getOldIssues(page);
        var resultCount = newResults.length;
        console.log('# of results: '+resultCount)
        listIssueResponse = listIssueResponse.concat(newResults);
        if ( resultCount == 100 ){
          page++
        }
        m++
      }
    }



        // Report JSON creation
        var i = 0;
        while (i < numberOfVulns) {
            var  refLink = results.records[0].vulnerabilities[i].libraries[0]._links.ref;
            var libRef = refLink.split("/");
            var coordinateFirst = results.records[0].libraries[libRef[4]].coordinate1;
            var coordinateSecond = results.records[0].libraries[libRef[4]].coordinate2;
            if ( results.records[0].vulnerabilities[i].cve == null ){
              var fndingsCVE = "none CVE"
            }
            else {
              var fndingsCVE = results.records[0].vulnerabilities[i].cve
            }

            //find filename
            var coordsLenght = results.records[0].graphs[0].directs
            var numberOfCoords = coordsLenght.length
            var k = 0;
            while ( k < numberOfCoords ){
              if ( results.records[0].graphs[0].directs[k].coords.coordinate1 == coordinateFirst && results.records[0].graphs[0].directs[k].coords.coordinate2 == coordinateSecond){
                var libFilename = results.records[0].graphs[0].directs[k].filename
                k = numberOfCoords
              }
              else {
                var libFilename = 'transitive dependency'
              }
              k++
            }

            //create links
            var vulnerabilityLinks = ''

            if ( typeof results.records[0].libraries[libRef[4]].versions[0]._links.html != 'undefined' ){
              var vulnerabilityLinks1 = results.records[0].libraries[libRef[4]].versions[0]._links.html
            }
            if ( typeof results.records[0].vulnerabilities[i]._links.html != 'undefined' && results.records[0].vulnerabilities[i]._links.html != null ){
              var vulnerabilityLinks2 = results.records[0].vulnerabilities[i]._links.html
            }
            else {
              var vulnerabilityLinks2 = "https://www.veracode.com"
            }
            if ( results.records[0].vulnerabilities[i].libraries[0].details[0].patch != '' && typeof results.records[0].vulnerabilities[i].libraries[0].details[0].patch != 'undefined' && results.records[0].vulnerabilities[i].libraries[0].details[0].patch != null ){
              var vulnerabilityLinks3 = results.records[0].vulnerabilities[i].libraries[0].details[0].patch
            }
            else {
              var vulnerabilityLinks3 = "https://www.veracode.com"
            }

            //check for fixText
            if ( typeof results.records[0].vulnerabilities[i].libraries[0].details[0].fixText != 'undefined' && results.records[0].vulnerabilities[i].libraries[0].details[0].fixText != '' && results.records[0].vulnerabilities[i].libraries[0].details[0].fixText != null){
              var myFixtext = results.records[0].vulnerabilities[i].libraries[0].details[0].fixText
            }
            else {
              var myFixtext = 'No immediate fix found!'
            }

            //check for bugTrackUrl
            if ( results.records[0].libraries[libRef[4]].bugTrackerUrl != null){
              var myBugTrackURL = results.records[0].libraries[libRef[4]].bugTrackerUrl
            }
            else {
              var myBugTrackURL = 'https://www.veracode.com'
            }


            var oldSeverity = parseInt(results.records[0].vulnerabilities[i].cvssScore);

            //severity mapping
            if (oldSeverity == '0.0')
              mapSeverity = 'Unknown'
            else if (oldSeverity >= '0.1' && oldSeverity < '3.9')
              mapSeverity = 'Low'
            else if (oldSeverity >= '4.0' && oldSeverity < '6.9')
              mapSeverity = 'Medium'
            else if (oldSeverity >= '7.0' && oldSeverity < '8.9')
              mapSeverity = 'High'
            else if (oldSeverity >= '9.0')
              mapSeverity = 'Critical'

            // construct Vulnerabilities for reports file
            vulns = {
                id: results.records[0].libraries[libRef[4]].versions[0].sha1,
                category: "dependency_scanning",
                name: results.records[0].vulnerabilities[i].title+' at '+results.records[0].libraries[libRef[4]].name,
                message: '',
                cve: fndingsCVE,
                description: results.records[0].libraries[libRef[4]].description+' - '+results.records[0].vulnerabilities[i].overview,
                severity: mapSeverity,
                solution: myFixtext,
                scanner: {
                    id: "Veracode Agent Based SCA",
                    name: "Veracode Agent Based SCA"
                  },
                  location: {
                    file: libFilename,
                    dependency: {
                      package: {
                        name: results.records[0].libraries[libRef[4]].coordinateType+':'+results.records[0].libraries[libRef[4]].coordinate1+':'+results.records[0].libraries[libRef[4]].coordinate2,
                      },
                      version: results.records[0].libraries[libRef[4]].versions[0].version
                    }
                  },
                  identifiers: [
                    {
                      type: "Veracode Agent Based SCA",
                      cve: fndingsCVE,
                      name: results.records[0].vulnerabilities[i].language+' - '+results.records[0].libraries[libRef[4]].name+' - Version: '+results.records[0].vulnerabilities[i].libraries[0].details[0].versionRange+' - CVE: '+results.records[0].vulnerabilities[i].cve,
                      value: results.records[0].vulnerabilities[i].language+' - '+results.records[0].libraries[libRef[4]].name+' - Version: '+results.records[0].vulnerabilities[i].libraries[0].details[0].versionRange+' - CVE: '+results.records[0].vulnerabilities[i].cve,
                      url: myBugTrackURL
                    }
                  ],
                  links: [
                    {
                      url: vulnerabilityLinks1
                    },
                    {
                      url: vulnerabilityLinks2
                    },
                    {
                      url: vulnerabilityLinks3
                    }
                  ]
            };

            var diffValue = Buffer.from(myFixtext).toString('base64')
            remeds = {
                              fixes: 
                              [
                                {
                                  cve: fndingsCVE,
                                  id: results.records[0].libraries[libRef[4]].versions[0].sha1
                                }
                              ],
                              summary: myFixtext,
                              diff: diffValue
                            };
            
            vulnerabilities.push(JSON.stringify(vulns));
            remediations.push(JSON.stringify(remeds));


            //issue section
            if (myArgs[1] == 'true'){

              // set a few vars
              var weight = Math.floor(results.records[0].vulnerabilities[i].cvssScore);

              // map CVSS to severity lable
              /*
              0 - Informational
              0.1 - 2 - Very Low
              2.1 - 4 - Low
              4.1 - 6 - Medium
              6.1 - 8 - High
              8.1 - 10 - Very High
              */

              if (weight == '0.0')
                  severityLabel = 'Informational'
              else if (weight >= '0.1' && weight < '1.9')
                  severityLabel = 'Very Low'
              else if (weight >= '2.0' && weight < '3.9')
                  severityLabel = 'Low'
              else if (weight >= '4.0' && weight < '5.9')
                  severityLabel = 'Medium'
              else if (weight >= '6.0' && weight < '7.9')
                  severityLabel = 'High'
              else if (weight >= '8.0')
                  severityLabel = 'Very High'


              if (results.records[0].vulnerabilities[i].cve == null){
                myCVE = '0000-0000';
              }
              else {
                myCVE = results.records[0].vulnerabilities[i].cve;
              }

              var title = "Dependency Issue - "+results.records[0].vulnerabilities[i].language+" - "+results.records[0].libraries[libRef[4]].name+" - Version: "+results.records[0].vulnerabilities[i].libraries[0].details[0].versionRange+" - CVE: "+myCVE;
              var label = "Dependency Scanning,"+myCVE+","+severityLabel;
              var description = "Software Composition Analysis  \n  \n  \nLanguage: "+results.records[0].vulnerabilities[i].language+"  \nLibrary: "+results.records[0].libraries[libRef[4]].name+"  \nCVE: "+results.records[0].vulnerabilities[i].cve+"  \nVersion: "+results.records[0].vulnerabilities[i].libraries[0].details[0].versionRange+"  \nDescription: "+results.records[0].libraries[libRef[4]].description+"  \n"+results.records[0].vulnerabilities[i].overview+"  \nFix: "+results.records[0].vulnerabilities[i].libraries[0].details[0].fixText+"  \nLinks: "+results.records[0].libraries[libRef[4]].versions[0]._links.html+"  \n"+results.records[0].vulnerabilities[i]._links.html+"  \n"+results.records[0].vulnerabilities[i].libraries[0].details[0].patch;

              // check for dublicated issues
              var existingIssues = listIssueResponse;
              //console.log(JSON.stringify(listIssueResponse))
              numberOfExisingIssues = existingIssues.length
              console.log('existing Issues: '+numberOfExisingIssues)
              j = 0;
              var exisitingFininding = false;
              while (j < numberOfExisingIssues){

                /*
                console.log(JSON.stringify(existingIssues[j]))
                console.log('interation: '+j)
                console.log(title)
                console.log(existingIssues[j].title)
                console.log(weight)
                console.log(existingIssues[j].weight)
                console.log(myCVE)
                console.log(existingIssues[j].labels[0])
                console.log(existingIssues[j].title+" - "+title+"\n"+existingIssues[j].weight+" - "+weight+"\n"+existingIssues[j].labels[0]+" - "+myCVE)
                */

                if (existingIssues[j].title == title && existingIssues[j].weight == weight && existingIssues[j].labels[0] == myCVE){
                  if(myArgs[3] == 'true'){
                    if(existingIssues[j].state == "closed"){
                      exisitingFininding = false;
                    }
                    else{
                      exisitingFininding = true;
                      j = numberOfExisingIssues
                    }
                  }
                  else {
                    exisitingFininding = true;
                    j = numberOfExisingIssues          
                  }
                }
              j++;
              }

              if (exisitingFininding != true){
                console.log("Issue needs to be created - "+title)

                  // create new issue
                  try {
                    var data = JSON.stringify({
                      title: title,
                      labels: label,
                      description: description,
                      weight: weight
                    })

                    var createIssueResposne = await Axios.request({
                      method: 'POST',
                      headers:{
                          'Content-Type': 'application/json',
                          'PRIVATE-TOKEN': myArgs[0]
                      },
                      data,
                      url: 'https://gitlab.com/api/v4/projects/'+myArgs[2]+'/issues'
                    });
                  }
                  catch (e) {
                    console.log(e)
                    console.log(e.response.data)
                  }

              }
              else {
                console.log('Issue already exists '+title)
              }
            }
            i++;
        }
        //vulns & remediations start
        var vulnsStart = '{"version": "14.0.0","dependency_files": [],"vulnerabilities":[';
        var remediationsStart = '"remediations": [';
        // vulns & remediations finish
        var vulnsEnd = ']';
        var remediationsEnd = ']}';
        //create full report
        var fullReportString = vulnsStart+vulnerabilities+vulnsEnd+','+remediationsStart+remediations+remediationsEnd
        var vulnerabilitiesReport = JSON.parse(fullReportString);
        //console.log('Vulnerabilities:'+fullReportString);


        // save to file
        fs.writeFileSync(outputFileName,fullReportString);
        console.log('Report file created: '+outputFileName);
}





//try {
    convertSCAResultFileToJSONReport(scaInputFileName,GitlabOutputFileName);
//} 
//catch (error) {
//    core.setFailed(error.message);
//}

module.exports = {
    converSCAResulst: convertSCAResultFileToJSONReport,
}
